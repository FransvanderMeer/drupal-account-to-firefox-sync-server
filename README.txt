Install FireFox Sync Server
See:
http://docs.services.mozilla.com/howtos/run-sync.html


Copy the drupal*.py files into
deps/server-core/services/user

Adept your sync.conf in etc/sync.conf

[auth]
allow_new_users = false
backend = services.user.drupalsql.DrupalSQLUser
# Database details for drupal.
sqluri = mysql://USER:PASS@localhost/DB
create_tables = false
encoding = utf-8
# no_pool = True
pool_size = 5
pool_recycle = 540

