""" Drupal SQL user management

Based on existing sql.py

"""

from sqlalchemy import create_engine, Integer, String
from sqlalchemy.interfaces import PoolListener
from sqlalchemy.ext.declarative import declarative_base, Column
from sqlalchemy.sql import bindparam, select, insert, update, delete
from sqlalchemy.exc import IntegrityError
from sqlalchemy.pool import NullPool

from services.util import (safe_execute)
from services.user import User, _password_to_credentials

from services.exceptions import BackendError
from drupalpass import DrupalHash


_Base = declarative_base()
tables = []


class Users(_Base):
    __tablename__ = 'users'

    uid = Column(Integer, primary_key=True, nullable=False)
    name = Column(String(60), unique=True, nullable=False)
    password = Column('pass', String(128))
    status = Column(Integer, default=1)
    mail = Column(String(254))

users = Users.__table__
tables.append(users)


_SQLURI = 'mysql://sync:sync@localhost/sync'

_USER_ID = select([Users.uid.label('userid')], users.c.name == bindparam('username'))

_USER_NAME = select([Users.name.label('username')], users.c.uid == bindparam('userid'))


class SetTextFactory(PoolListener):
    """This ensures strings are not converted to unicode on queries
    when using SQLite
    """
    def connect(self, dbapi_con, con_record):
        dbapi_con.text_factory = str


class DrupalSQLUser(object):
    """SQL authentication."""

    def __init__(self, sqluri=_SQLURI, pool_size=20, pool_recycle=60,
                 check_account_state=True, create_tables=True, no_pool=False,
                 allow_new_users=True, **kw):
        sqlkw = {'logging_name': 'weaveserver'}
        if sqluri.startswith('sqlite'):
            sqlkw['listeners'] = [SetTextFactory()]
        else:
            if not no_pool:
                sqlkw['pool_size'] = int(pool_size)
                sqlkw['pool_recycle'] = int(pool_recycle)
        if no_pool or sqluri.startswith('sqlite'):
            sqlkw['poolclass'] = NullPool

        self.check_account_state = check_account_state
        self.allow_new_users = allow_new_users
        self._engine = create_engine(sqluri, **sqlkw)
        users.metadata.bind = self._engine
        if create_tables:
            users.create(checkfirst=True)
        self.sqluri = sqluri

    def get_user_id(self, user):
        """Returns the id for a user name"""
        user_id = user.get('userid')
        if user_id is not None:
            return user_id

        username = user['username']
        if username is None:
            return None

        res = safe_execute(self._engine, _USER_ID,
                           username=username).fetchone()
        if res is None:
            return None
        user['userid'] = res.userid
        return res.userid

    def create_user(self, username, password, email):
        """Creates a user. Returns True on success."""
        if not self.allow_new_users:
            raise BackendError("Creation of new users is disabled")
        hasher = DrupalHash()
        password_hash = hasher.password_crypt('sha512', password.encode('utf8'), hasher.password_generate_salt(15))
        query = insert(users).values({'name': username, 'mail': email,
                                     'pass': password_hash,
                                     'status': 1})
        try:
            res = safe_execute(self._engine, query)
        except IntegrityError:
            #Name already exists
            return False

        if res.rowcount != 1:
            return False

        #need a copy with some of the info for the return value
        userobj = User()
        userobj['username'] = username
        userobj['userid'] = res.lastrowid
        userobj['mail'] = email

        return userobj

    @_password_to_credentials
    def authenticate_user(self, user, credentials, attrs=None):
        """Authenticates a user given a user object and credentials.

        Returns the user id in case of success. Returns None otherwise.
        """

        username = credentials.get("username")
        if username is None:
            return None
        if user.get("username") not in (None, username):
            return None

        password = credentials.get("password")
        if password is None:
            return None

        fields = [users.c.uid, Users.password.label('password'), users.c.status]
        if attrs is not None:
            for attr in attrs:
                fields.append(getattr(users.c, attr))
        else:
            attrs = []

        _USER_AUTH = select(fields, users.c.name == bindparam('username'))
        res = safe_execute(self._engine, _USER_AUTH,
                           username=username).fetchone()
        if res is None:
            return None

        if self.check_account_state and res.status != 1:
            return None

        hasher = DrupalHash()
        if hasher.password_crypt('sha512', password.encode('utf8'), res.password) != res.password:
            return None

        user['username'] = username
        user['userid'] = res.uid
        for attr in attrs:
            user[attr] = getattr(res, attr)
        return res.uid

    def get_user_info(self, user, attrs):
        """Returns user info

        Args:
            user: the user object
            attrs: the pieces of data requested

        Returns:
            user object populated with attrs
        """
        user_id = self.get_user_id(user)
        if user_id is None:
            return user

        attrs = [attr for attr in attrs if not user.get(attr)]
        if attrs == []:
            return user

        fields = []
        for attr in attrs:
            fields.append(getattr(users.c, attr))

        _USER_INFO = select(fields, users.c.uid == bindparam('user_id'))
        res = safe_execute(self._engine, _USER_INFO,
                           user_id=user_id).fetchone()
        if res is None:
            return user
        for attr in attrs:
            try:
                user[attr] = getattr(res, attr)
            except AttributeError:
                user[attr] = None

        return user

    @_password_to_credentials
    def update_field(self, user, credentials, key, value):
        """Change the value of a user's field
        True if the change was successful, False otherwise
        """
        if not self.authenticate_user(user, credentials):
            return False

        #we don't have the concept of differently permissioned users here
        return self.admin_update_field(user, key, value)

    def admin_update_field(self, user, key, value):
        """Change the value of a user's field using an admin bind
        True if the change was successful, False otherwise

        The sql model doesn't have the concept of different permissions,
        """
        user_id = self.get_user_id(user)
        if user_id is None:
            return False

        query = update(users, users.c.uid == user_id, {key: value})
        res = safe_execute(self._engine, query)
        user[key] = value
        return res.rowcount == 1

    @_password_to_credentials
    def update_password(self, user, credentials, new_password):
        """
        Change the user password. Uses the user bind.

        Args:
            user: user object
            credentials: a dict containing the user's auth credentials
            old_password: old password of the user

        Returns:
            True if the change was successful, False otherwise
        """
        if not self.authenticate_user(user, credentials):
            return False

        #we don't have the concept of differently permissioned users here
        return self.admin_update_password(user, new_password)

    def admin_update_password(self, user, new_password, code=None):
        """Change the user password

        Args:
            user_id: user id
            password: new password

        Returns:
            True if the change was successful, False otherwise
        """
        hasher = DrupalHash()
        password_hash = hasher.password_crypt('sha512', new_password.encode('utf8'), hasher.password_generate_salt(15))
        return self.admin_update_field(user, 'password', password_hash)

    @_password_to_credentials
    def delete_user(self, user, credentials=None):
        """
        Deletes a user. Needs to be done with admin privileges, since
        users don't have permission to do it themselves.

        Args:
            user: the user object

        Returns:
            True if the deletion was successful, False otherwise
        """

        if credentials is not None:
            if not self.authenticate_user(user, credentials):
                return False

        user_id = self.get_user_id(user)
        if user_id is None:
            return False

        query = delete(users).where(users.c.uid == user_id)
        res = safe_execute(self._engine, query)
        return res.rowcount == 1
